package ru.t1.aksenova.tm.model;

public abstract class AbstractUserOwnerModel extends AbstractModel {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
