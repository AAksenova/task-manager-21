package ru.t1.aksenova.tm.component;

import ru.t1.aksenova.tm.api.repository.ICommandRepository;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.command.project.*;
import ru.t1.aksenova.tm.command.system.*;
import ru.t1.aksenova.tm.command.task.*;
import ru.t1.aksenova.tm.command.user.*;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.aksenova.tm.exception.system.CommandNotSupportedException;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.repository.CommandRepository;
import ru.t1.aksenova.tm.repository.ProjectRepository;
import ru.t1.aksenova.tm.repository.TaskRepository;
import ru.t1.aksenova.tm.repository.UserRepository;
import ru.t1.aksenova.tm.service.*;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IUserRepository userRepository = new UserRepository();


    private final ICommandService commandService = new CommandService(commandRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILoggerService loggerService = new LoggerService();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());

        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());

        registry(new UserRegistryCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    public void start(final String[] args) {
        if (processArguments(args)) System.exit(0);

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                super.run();
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void initDemoData() {
        final User test = userService.create("test", "test", "test@test.ru");
        final User user = userService.create("user", "user", "user@user.ru");
        final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("One test project", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("Two test project", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("Three test project", Status.COMPLETED));
        projectService.add(admin.getId(), new Project("Four test project", Status.NOT_STARTED));
        projectService.add(admin.getId(), new Project("Five test project", Status.COMPLETED));
        projectService.add(user.getId(), new Project("Six test project", Status.NOT_STARTED));

        taskService.add(test.getId(), new Task("Alfa task", Status.IN_PROGRESS));
        taskService.add(test.getId(), new Task("Beta task", Status.COMPLETED));
        taskService.add(test.getId(), new Task("Gamma task", Status.NOT_STARTED));
        taskService.add(admin.getId(), new Task("Delta task", Status.IN_PROGRESS));
        taskService.add(admin.getId(), new Task("Eta task", Status.NOT_STARTED));
        taskService.add(user.getId(), new Task("Teta task", Status.IN_PROGRESS));
    }

}
