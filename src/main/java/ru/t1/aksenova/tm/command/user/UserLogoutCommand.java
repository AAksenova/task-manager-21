package ru.t1.aksenova.tm.command.user;

import ru.t1.aksenova.tm.api.service.IAuthService;
import ru.t1.aksenova.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "logout";

    public static final String DESCRIPTION = "Logout current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        final IAuthService authService = serviceLocator.getAuthService();
        authService.logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
