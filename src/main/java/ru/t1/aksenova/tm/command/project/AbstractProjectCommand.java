package ru.t1.aksenova.tm.command.project;

import ru.t1.aksenova.tm.api.service.IProjectService;
import ru.t1.aksenova.tm.api.service.IProjectTaskService;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("PROJECT ID: " + project.getId());
        System.out.println("PROJECT NAME: " + project.getName());
        System.out.println("PROJECT DESCRIPTION: " + project.getDescription());
        System.out.println("PROJECT STATUS: " + Status.toName(project.getStatus()));
    }

}
