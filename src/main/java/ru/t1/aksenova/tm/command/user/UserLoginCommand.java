package ru.t1.aksenova.tm.command.user;

import ru.t1.aksenova.tm.api.service.IAuthService;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    public static final String NAME = "login";

    public static final String DESCRIPTION = "User login.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        authService.login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
