package ru.t1.aksenova.tm.command.task;

import ru.t1.aksenova.tm.api.service.IProjectTaskService;
import ru.t1.aksenova.tm.api.service.ITaskService;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("TASK ID: " + task.getId());
        System.out.println("TASK NAME: " + task.getName());
        System.out.println("TASK DESCRIPTION: " + task.getDescription());
        System.out.println("TASK STATUS: " + Status.toName(task.getStatus()));
        System.out.println("TASK PROJECT ID: " + task.getProjectId());
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index);
            showTask(task);
            index++;
            System.out.println("---------------------");
        }
    }

}
