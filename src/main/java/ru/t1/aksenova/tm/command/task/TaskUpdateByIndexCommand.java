package ru.t1.aksenova.tm.command.task;

import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-update-by-index";

    public static final String DESCRIPTION = "Update task by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        getTaskService().updateByIndex(userId, index, name, description);
    }

}
