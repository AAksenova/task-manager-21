package ru.t1.aksenova.tm.command.system;

import ru.t1.aksenova.tm.api.service.ICommandService;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
